//  We've created an App object (a set of key value pairs) to hold the applcation code.
//  The App object shows how to create a JavaScript object and includes
//  examples of standard programming constructs in JavaScript. 
//  The goal is provide many useful examples in a single code file. 
//
//  When you modify the application, use different ids for your HTML elements.
//  Do not use length and width. 

$('#outputDiv').hide();

var App = {
  launch: function () {
    App.getOrganizationName();
    App.getTypeOfEvent();
    App.getNoOfRows();
    App.getSeatsPerRow();
    App.getTotalNumberOfSeats();
    App.displayExploreButtons();
    App.showExample();
  },
  getOrganizationName: function () {
    let answer = prompt("What is your organization name?", "Virtusa");
    if (answer != null) {
      $("#org").html(answer);
    }
  },
  getTypeOfEvent: function () {
    let answer = prompt("What is your event type?", "baseball");
    if (answer != null) {
      $("#event").html(answer);
    }
  },
  getNoOfRows: function () {
    let answer = prompt("How many rows you want to reserve?", 5);
    if (answer != null) {
      $('#noRows').html(answer);   // either double or single tick marks designate strings
    }
  },
  getSeatsPerRow: function () {
    let answer = prompt("How many seats per row?", 5);
    if (answer != null) {
      $('#seatsPerRow').html(answer);  // html method works as a getter and a setter
    }
  },
  getTotalNumberOfSeats: function () {
    let noOfRows = parseFloat($('#noRows').html());
    let noOfSeatsPerRow = parseFloat($('#seatsPerRow').html());
    let answer = App.calculateTotalSeats(noOfRows, noOfSeatsPerRow); // do some checks on the inputs
    $("#totalSeats").html(answer);
    $(".displayText").css('display', 'inline-block');  //overwrites display: hidden to make it visible 
    alert("You have selected " + answer + " seats.");
  },
  calculateTotalSeats: function (noOfRows, noOfSeatsPerRow) {
    if (typeof noOfRows !== 'number' || typeof noOfSeatsPerRow !== 'number') {
      throw Error('The given argument is not a number');
    }
    return noOfRows * noOfSeatsPerRow;
  },
  showExample: function () {
    document.getElementById("displaySeatPos").innerHTML = "";
    let noOfRows = parseFloat($('#noRows').html());
    let noOfSeatsPerRow = parseFloat($('#seatsPerRow').html());
    for (var i = 0; i < noOfRows; i++) {
      for (var j = 0; j < noOfSeatsPerRow; j++) {
        App.addImage(i,j);
      }
      var displayElement = document.getElementById("displaySeatPos");
      displayElement.appendChild(document.createElement("br"));
    }
    $('#outputDiv').show();
  },
  addImage: function (i,j) {
    var imageElement = document.createElement("img");
    imageElement.id = "image" + i+j;
    imageElement.class = "picture";
    imageElement.style.maxWidth = "90px";
    var displayElement = document.getElementById("displaySeatPos");
    displayElement.appendChild(imageElement);
    document.getElementById("image" + i+j).src = "bearcat-logo.png";
  },
  displayExploreButtons: function () {
    $(".displayExploreButtons").css('display', 'block');  //overwrites display: hidden to make it visible 
  },
  exploreHtml: function () {
    alert("Would you like to learn more? \n\n Run the app in Chrome.\n\n" +
      "Right-click on the page, and click Inspect. Click on the Elements tab.\n\n" +
      "Hit CTRL-F and search for displaySeatPos to see the new image elements you added to the page.\n")
  },
  exploreCode: function () {
    alert("Would you like explore the running code? \n\n Run the app in Chrome.\n\n" +
      "Right-click on the page, and click Inspect. Click on the top-level Sources tab.\n\n" +
      "In the window on the left, click on the .js file.\n\n" +
      "In the window in the center, click on the line number of the App.getFirstName() call to set a breakpoint.\n\n" +
      "Click on it again to remove the breakpoint, and one more time to turn it back on.\n\n" +
      "Up on the web page, click the main button to launch the app.\n\n" +
      "Execution of the code will stop on your breakpoint.\n\n" +
      "Hit F11 to step into the App.getFirstName() function.\n" +
      "Hit F10 to step over the next function call.\n\n" +
      "As you hit F11 and step through your code, the values of local variables appear beside your code - very helpful in debugging.\n\n" +
      "Caution: Hitting F11 in VS Code will make your top-level menu disapper. Hit F11 again to bring it back.\n"
    )
  }
};

